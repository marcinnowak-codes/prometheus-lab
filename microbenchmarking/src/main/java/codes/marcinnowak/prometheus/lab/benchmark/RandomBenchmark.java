package codes.marcinnowak.prometheus.lab.benchmark;

import codes.marcinnowak.prometheus.lab.calculation.PromTimeStamp;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import codes.marcinnowak.prometheus.lab.simulator.PrometheusSimulatorRandom;
import codes.marcinnowak.prometheus.lab.simulator.PrometheusSimulatorRandomInitial;
import org.apache.commons.lang3.tuple.Pair;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static codes.marcinnowak.prometheus.lab.Utility.sample;
import static codes.marcinnowak.prometheus.lab.promql.PromQlVisitorReversePolishNotationCalculation.increase;
import static codes.marcinnowak.prometheus.lab.promql.PromQlVisitorReversePolishNotationCalculation.increaseNormalize;

public class RandomBenchmark {

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    public void shouldIncreaseRandom(Blackhole blackhole, IncreaseRandomState state) {

        List<Pair<PromTimeStamp, BigDecimal>> increase = increase(state.samples, state.interval, state.step, state.start, state.end);

        blackhole.consume(increase);
    }

    @Benchmark
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    @BenchmarkMode(Mode.AverageTime)
    public void shouldIncreaseNormalizeRandom(Blackhole blackhole, IncreaseRandomState state) {

        List<Pair<PromTimeStamp, BigDecimal>> increase = increaseNormalize(state.samples, state.interval, state.step, state.start, state.end);

        blackhole.consume(increase);
    }

    @State(Scope.Thread)
    public static class IncreaseRandomState {
        @Param({ "100", "1000", "10000"})
        public long sampleSize;

        public PromTimeStamp start = PromTimeStamp.of("1600000001");
        public PromTimeStamp end;
        public int step = 1;
        public BigDecimal interval = BigDecimal.valueOf(1);
        private List<Pair<PromTimeStamp, BigDecimal>> samples;

        @Setup(Level.Invocation)
        public void setUp() {
            end = PromTimeStamp.of(start.toBigDecimal().longValue() + sampleSize);
            PrometheusSimulatorRandom prometheusSimulatorRandom;
            if (Math.random() > 0.5)
                prometheusSimulatorRandom = PrometheusSimulatorRandomInitial.create(5);
            else
                prometheusSimulatorRandom = PrometheusSimulatorRandom.create(5);

            final PrometheusQueryRangeResponse rangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                    .query("http_requests")
                    .start(start.toBigDecimal())
                    .end(end.toBigDecimal())
                    .step(step)
                    .build());

            samples = rangeResponse.getData().getResults().get(0).getValues().stream()
                    .map(objects -> sample((long) objects.get(0), (String) objects.get(1)))
                    .collect(Collectors.toList());
        }

    }

}
