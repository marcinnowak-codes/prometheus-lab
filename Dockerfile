FROM openjdk:13-jdk-alpine
ARG JAR_PATH
ARG JAR_FILE=${JAR_PATH}/target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]
