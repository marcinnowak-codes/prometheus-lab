package codes.marcinnowak.prometheus.lab.promql;

public class SampleTimeConversationException extends RuntimeException {
    public SampleTimeConversationException(String message) {
        super(message);
    }
}
