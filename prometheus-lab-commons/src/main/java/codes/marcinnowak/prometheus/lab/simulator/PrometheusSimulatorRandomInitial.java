package codes.marcinnowak.prometheus.lab.simulator;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static com.github.cowwoc.requirements.DefaultRequirements.requireThat;

public class PrometheusSimulatorRandomInitial implements PrometheusSimulator {

    private final Random generator;
    private final double increaseRange;

    public PrometheusSimulatorRandomInitial(int increaseRange, long seed) {
        requireThat(increaseRange, "increaseRange").isPositive();

        generator = new Random(seed);
        this.increaseRange = increaseRange;
    }

    public static PrometheusSimulatorRandom create(int increase){
        return new PrometheusSimulatorRandom(increase, System.currentTimeMillis());
    }

    @Override
    public PrometheusQueryRangeResponse queryRange(PrometheusQueryRequest promQueryRange) {
        requireThat(promQueryRange.getStart(), "start").isLessThan(promQueryRange.getEnd());
        requireThat(promQueryRange.getStep(), "step").isGreaterThan(0);

        final int start = promQueryRange.getStart().intValue();
        final int end = promQueryRange.getEnd().intValue();
        final int step = promQueryRange.getStep();

        List<List<Object>> samples = new ArrayList<>();

        long previous = (long) (increaseRange * generator.nextDouble());
        long increase = (long) (increaseRange * generator.nextDouble());

        for (long t = start; t <= end; t += step) {
            previous += increase;
            samples.add(Arrays.asList(t, String.valueOf(previous)));
        }

        return PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .values(samples)
                                .build())
                        .build())
                .build();
    }
}
