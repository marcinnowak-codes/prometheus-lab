package codes.marcinnowak.prometheus.lab.promql;

import lombok.Getter;
import org.antlr.v4.runtime.tree.ParseTree;

import java.util.LinkedList;
import java.util.List;

public class PromQlVisitorReverse extends PromQlBaseVisitor<Void> {

    @Getter
    private List<ParseTree> contexts = new LinkedList<>();

    @Override
    public Void visitIntervalAtom(PromQlParser.IntervalAtomContext ctx) {
        contexts.add(0, ctx);
        super.visitIntervalAtom(ctx);
        return null;
    }

    @Override
    public Void visitIntervalAtomWithFilter(PromQlParser.IntervalAtomWithFilterContext ctx) {
        contexts.add(0, ctx);
        return null;
    }

    @Override
    public Void visitAtom(PromQlParser.AtomContext ctx) {
        contexts.add(0, ctx);
        super.visitAtom(ctx);
        return null;
    }

    @Override
    public Void visitFunc(PromQlParser.FuncContext ctx) {
        contexts.add(0, ctx);
        super.visitFunc(ctx);
        return null;
    }


    @Override
    public Void visitSumBy(PromQlParser.SumByContext ctx) {
        contexts.add(0, ctx);
        super.visitSumBy(ctx);
        return null;
    }

    @Override
    public Void visitSumByExpresion(PromQlParser.SumByExpresionContext ctx) {
        return null;
    }
}
