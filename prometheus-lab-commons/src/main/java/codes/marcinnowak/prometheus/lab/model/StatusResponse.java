package codes.marcinnowak.prometheus.lab.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class StatusResponse {
    private final String status;
    private final DataResponse data;

    @Data
    @Builder
    public static class DataResponse {
        private final String resultType;
        @Singular
        @JsonProperty("result")
        private final List<Object> results;
    }
}
