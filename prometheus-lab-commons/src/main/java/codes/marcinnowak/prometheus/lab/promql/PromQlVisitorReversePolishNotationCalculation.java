package codes.marcinnowak.prometheus.lab.promql;

import codes.marcinnowak.prometheus.lab.calculation.PromTimeStamp;
import codes.marcinnowak.prometheus.lab.calculation.PrometheusQueryRangeResponseCalculation;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import lombok.Getter;
import org.antlr.v4.runtime.RuleContext;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;


public class PromQlVisitorReversePolishNotationCalculation extends PromQlBaseVisitor<PromEvent> {

    private final PrometheusQueryRequest prometheusQueryRequest;
    @Getter
    private PrometheusQueryRangeResponse prometheusQueryRangeResponse;
    private Integer interval = 60;
    private PrometheusQueryRequest promQueryRange;
    private PromQlParser.FuncContext functionContext;

    public PromQlVisitorReversePolishNotationCalculation(PrometheusQueryRequest prometheusQueryRequest) {

        this.prometheusQueryRequest = prometheusQueryRequest;
    }

    private static PromTimeStamp toSampleTime(Object sampleTime) {
        if (sampleTime instanceof Integer) {
            return PromTimeStamp.of(new BigDecimal((Integer) sampleTime));
        } else if (sampleTime instanceof Long) {
            return PromTimeStamp.of(new BigDecimal((Long) sampleTime));
        } else if (sampleTime instanceof Double) {
            return PromTimeStamp.of(BigDecimal.valueOf((Double) sampleTime));
        }else if (sampleTime instanceof BigDecimal){
            return PromTimeStamp.of(sampleTime);
        } else {
            throw new SampleTimeConversationException("Can't convert sample time class: " + sampleTime.getClass().getCanonicalName());
        }
    }

    @Override
    public PromEvent visitIntervalAtom(PromQlParser.IntervalAtomContext ctx) {
        if (ctx.SCIENTIFIC_NUMBER().size() == 2 && "*".equals(ctx.TIMES().getText()))
            interval = toInterval(ctx.timeUnit().getText(), Integer.parseInt(ctx.SCIENTIFIC_NUMBER().get(0).getText()) * Integer.parseInt(ctx.SCIENTIFIC_NUMBER().get(1).getText()));
        else
            interval = toInterval(ctx.timeUnit().getText(), Integer.parseInt(ctx.SCIENTIFIC_NUMBER().get(0).getText()));
        return new PromEventNull();
    }

    private static int toInterval(String unit, int interval) {
        switch (unit) {
            case "s":
                return interval;
            case "m":
                return 60 * interval;
            case "h":
                return 3600 * interval;
            default:
                throw new NotSupportedIntervalUnit("Not supported: " + unit);
        }
    }

    @Override
    public PromEvent visitIntervalAtomWithFilter(PromQlParser.IntervalAtomWithFilterContext ctx) {
        if (ctx.SCIENTIFIC_NUMBER().size() == 2 && "*".equals(ctx.TIMES().getText()))
            interval = toInterval(ctx.timeUnit().getText(), Integer.parseInt(ctx.SCIENTIFIC_NUMBER().get(0).getText()) * Integer.parseInt(ctx.SCIENTIFIC_NUMBER().get(1).getText()));
        else
            interval = toInterval(ctx.timeUnit().getText(), Integer.parseInt(ctx.SCIENTIFIC_NUMBER().get(0).getText()));

        promQueryRange = PrometheusQueryRequest.builder()
                .query(ctx.atom().getText() + "{" + serializeFilter(ctx.filterParameters()) + "}")
                .start(prometheusQueryRequest.getStart())
                .end(prometheusQueryRequest.getEnd())
                .step(prometheusQueryRequest.getStep())
                .build();
        return new PromEventNull();
    }

    private String serializeFilter(List<PromQlParser.FilterParametersContext> filterParametersContexts) {
        return filterParametersContexts.stream()
                .map(ctx -> ctx.variable().getText() + ctx.filterRelation().getText() +
                        ctx.filterValue().FILTER_VARIABLE().getText())
                .collect(Collectors.joining(","));
    }

    @Override
    public PromEvent visitAtom(PromQlParser.AtomContext ctx) {
        promQueryRange = PrometheusQueryRequest.builder()
                .query(ctx.getText())
                .start(prometheusQueryRequest.getStart())
                .end(prometheusQueryRequest.getEnd())
                .step(prometheusQueryRequest.getStep())
                .build();
        return new PromEventNull();
    }

    @Override
    public PromEvent visitFunc(PromQlParser.FuncContext ctx) {
        functionContext = ctx;
        if (Objects.isNull(prometheusQueryRangeResponse)) {
            return PromDataRequest.builder()
                    .query(promQueryRange.getQuery())
                    .start(promQueryRange.getStart())
                    .end(promQueryRange.getEnd())
                    .step(prometheusQueryRequest.getStep())
                    .underline(promQueryRange.getUnderline())
                    .build();
        }

        return onDataRequestResponseProcess(prometheusQueryRangeResponse.getData());
    }

    public PromEventNull onDataRequestResponse(PrometheusQueryRangeResponse response) {
        prometheusQueryRangeResponse = response;
        return onDataRequestResponseProcess(prometheusQueryRangeResponse.getData());
    }

    public PromEventNull onDataRequestResponseProcess(PrometheusQueryRangeResponse.ResponseData data) {
        Validate.notNull(functionContext);

        switch (functionContext.funcname().getText()) {
            case "lab_increase":
                for (PrometheusQueryRangeResponse.ResponseData.ResponseResult result : data.getResults()) {
                    List<Pair<PromTimeStamp, BigDecimal>> samples = result.getValues().stream()
                            .map(objects -> Pair.of(toSampleTime(objects.get(0)), new BigDecimal((String) objects.get(1))))
                            .collect(Collectors.toList());

                    List<Pair<PromTimeStamp, BigDecimal>> results = increase(samples,
                            BigDecimal.valueOf(interval),
                            prometheusQueryRequest.getStep(),
                            PromTimeStamp.of(prometheusQueryRequest.getStart()),
                            PromTimeStamp.of(prometheusQueryRequest.getEnd()));

                    List<List<Object>> increase = results.stream()
                            .map(sampleTimeBigDecimalPair -> Arrays.asList((Object) sampleTimeBigDecimalPair.getKey().toBigDecimal(),
                                    String.valueOf(sampleTimeBigDecimalPair.getValue().doubleValue())))
                            .collect(Collectors.toList());

                    result.setValues(increase);
                }
                break;
            case "lab_nincrease":
                for (PrometheusQueryRangeResponse.ResponseData.ResponseResult result : data.getResults()) {
                    List<Pair<PromTimeStamp, BigDecimal>> samples = result.getValues().stream()
                            .map(objects -> Pair.of(toSampleTime(objects.get(0)), new BigDecimal((String) objects.get(1))))
                            .collect(Collectors.toList());

                    List<Pair<PromTimeStamp, BigDecimal>> results = increase(samples,
                            BigDecimal.valueOf(interval),
                            prometheusQueryRequest.getStep(),
                            PromTimeStamp.of(prometheusQueryRequest.getStart()),
                            PromTimeStamp.of(prometheusQueryRequest.getEnd()));
                    results = increaseNormalize(results,
                            BigDecimal.valueOf(interval),
                            prometheusQueryRequest.getStep(),
                            PromTimeStamp.of(prometheusQueryRequest.getStart()),
                            PromTimeStamp.of(prometheusQueryRequest.getEnd()));
                    List<List<Object>> increase = results.stream()
                            .map(sampleTimeBigDecimalPair -> Arrays.asList((Object) sampleTimeBigDecimalPair.getKey().toBigDecimal(),
                                    String.valueOf(sampleTimeBigDecimalPair.getValue().doubleValue())))
                            .collect(Collectors.toList());

                    result.setValues(increase);
                }
                break;
            case "sum":
                PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation responseResultCalculation = calculateSum(toResponseResultCalculations(data.getResults()));

                PrometheusQueryRangeResponse.ResponseData.ResponseResult responseResult = toResponseResult(responseResultCalculation);

                data.setResults(Collections.singletonList(responseResult));
                break;
            default:
                throw new PromQlStatementNotSupported(functionContext.funcname().getText());
        }
        return new PromEventNull();
    }

    private static PrometheusQueryRangeResponse.ResponseData.ResponseResult toResponseResult(PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation responseResultCalculation) {
        return PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                .metrics(responseResultCalculation.getMetrics())
                .values(toResponseResultValues(responseResultCalculation.getValues()))
                .build();
    }

    private static List<List<Object>> toResponseResultValues(List<Pair<PromTimeStamp, BigDecimal>> values) {
        return values.stream()
                .<List<Object>>map(sample -> Arrays.asList(sample.getKey().toBigDecimal(), sample.getValue().toString()))
                .collect(Collectors.toList());
    }

    private static PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation calculateSum(List<PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation> responseDataResultsCalculation) {

        PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation.ResponseResultBuilder sumBuilder = PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation.builder()
                .metrics(responseDataResultsCalculation.get(0).getMetrics());

        Set<BigDecimal> timeStampGlobal = new HashSet<>();

        List<Map<BigDecimal, BigDecimal>> semiResult = new ArrayList<>();
        for (int sampleIndex = 0; sampleIndex < responseDataResultsCalculation.size(); sampleIndex++) {
            semiResult.add(new HashMap<>());
        }

        for (int variableIndex = 0; variableIndex < responseDataResultsCalculation.size(); variableIndex++) {
            PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation responseResultCalculation = responseDataResultsCalculation.get(variableIndex);
            for (int sampleIndex = 0; sampleIndex < responseResultCalculation.getValues().size(); sampleIndex++) {
                Pair<PromTimeStamp, BigDecimal> sample = responseResultCalculation.getValues().get(sampleIndex);
                BigDecimal value = sample.getValue();

                semiResult.get(variableIndex).put(sample.getKey().toBigDecimal(), value);
                timeStampGlobal.add(sample.getKey().toBigDecimal());
            }
        }

        Map<BigDecimal, BigDecimal> result = new HashMap<>();

        BigDecimal[] timeStampsSorted = timeStampGlobal.toArray(new BigDecimal[0]);
        Arrays.sort(timeStampsSorted);

        for (BigDecimal timestamp : timeStampsSorted) {

            for (int variableIndex = 0; variableIndex < semiResult.size(); variableIndex++) {
                result.putIfAbsent(timestamp, BigDecimal.ZERO);
                int finalVariableIndex = variableIndex;
                result.compute(timestamp, (bigDecimal, value) -> value.add(semiResult.get(finalVariableIndex).getOrDefault(timestamp, BigDecimal.ZERO)));
            }
        }

        for (BigDecimal timeStamp : timeStampsSorted) {
            sumBuilder.value(PromTimeStamp.of(timeStamp), result.get(timeStamp));
        }

        return sumBuilder.build();
    }

    private static List<PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation> toResponseResultCalculations(List<PrometheusQueryRangeResponse.ResponseData.ResponseResult> responseDataResults) {
        return responseDataResults.stream()
                .map(responseResult2 -> PrometheusQueryRangeResponseCalculation.ResponseDataCalculation.ResponseResultCalculation.builder()
                        .metrics(responseResult2.getMetrics())
                        .values(responseResult2.getValues().stream()
                                .map(objects -> Pair.of(PromTimeStamp.of((BigDecimal) objects.get(0)), new BigDecimal((String) objects.get(1))))
                                .collect(Collectors.toList()))
                        .build())
                .collect(Collectors.toList());
    }

    @Override
    public PromEvent visitSumBy(PromQlParser.SumByContext ctx) {
        List<String> groupsBy = ctx.sumByExpresion().expression().stream()
                .map(RuleContext::getText)
                .collect(Collectors.toList());
        List<PrometheusQueryRangeResponse.ResponseData> subgroups = PQLParser.extractSubgroups(prometheusQueryRangeResponse.getData(), groupsBy);

        List<PrometheusQueryRangeResponse.ResponseData.ResponseResult> result = subgroups.stream()
                .map(responseData -> toResponseResult(calculateSum(toResponseResultCalculations(responseData.getResults()))))
                .collect(Collectors.toList());

        prometheusQueryRangeResponse.getData().setResults(result);
        return new PromEventNull();
    }

    /**
     * @param samples  list of pair of timestamp and value
     * @param interval
     * @param step
     * @param start
     * @param end
     * @return
     */
    public static List<Pair<PromTimeStamp, BigDecimal>> increase(List<Pair<PromTimeStamp, BigDecimal>> samples,
                                                                 BigDecimal interval,
                                                                 int step,
                                                                 PromTimeStamp start,
                                                                 PromTimeStamp end) {

        List<Pair<PromTimeStamp, BigDecimal>> samplesContinuous = makeContinuousAfterRestart(samples, step, start, end);

        List<Pair<PromTimeStamp, BigDecimal>> result = new ArrayList<>();
        BigDecimal previousValue = samplesContinuous.get(0).getValue();

        PromTimeStamp intervalBeginTime = start;
        List<Pair<PromTimeStamp, BigDecimal>> samplesBelongToInterval = new ArrayList<>();

        int i = 0;
        BigDecimal increase = BigDecimal.ZERO;
        boolean missingDataBegin = start.isEarlierThen(samplesContinuous.get(i).getKey());

        for (PromTimeStamp t = start; t.isEarlierOrEquals(end); t = t.plusSeconds(step)) {
            //Handle missing data
            if (t.isEarlierThen(samplesContinuous.get(i).getKey())) {
                if (!samplesBelongToInterval.isEmpty()) {
                    BigDecimal finalIncrease = increase;
                    result.addAll(samplesBelongToInterval.stream()
                            .map(promTimeStamp -> Pair.of(promTimeStamp.getKey(), finalIncrease))
                            .collect(Collectors.toList()));
                    samplesBelongToInterval.clear();
                    intervalBeginTime = t;
                    increase = BigDecimal.ZERO;
                }
                intervalBeginTime = intervalBeginTime.plusSeconds(interval).equals(t) ? t : intervalBeginTime;
                continue;
            }

            // Handle counter restart in the begin
            increase = increase.add(samplesContinuous.get(i).getValue());
            // Head of sample
            increase = missingDataBegin ? increase : increase.subtract(previousValue);

            missingDataBegin = false;

            samplesBelongToInterval.add(samplesContinuous.get(i));
            previousValue = samplesContinuous.get(i).getValue();

            if (intervalBeginTime.plusSeconds(interval).equals(t) || i >= samplesContinuous.size() - 1) {

                BigDecimal finalIncrease = increase;
                result.addAll(samplesBelongToInterval.stream()
                        .map(promTimeStamp -> Pair.of(promTimeStamp.getKey(), finalIncrease))
                        .collect(Collectors.toList()));
                samplesBelongToInterval.clear();
                intervalBeginTime = t;
                increase = BigDecimal.ZERO;
            }
            i++;

        }
        return result;
    }

    public static List<Pair<PromTimeStamp, BigDecimal>> increaseNormalize(List<Pair<PromTimeStamp, BigDecimal>> samples,
                                                                          BigDecimal interval,
                                                                          int step,
                                                                          PromTimeStamp start,
                                                                          PromTimeStamp end) {

        List<Pair<PromTimeStamp, BigDecimal>> result = new ArrayList<>();

        PromTimeStamp intervalBeginTime = start;
        List<Pair<PromTimeStamp, BigDecimal>> samplesBelongToInterval = new ArrayList<>();

        int i = 0;

        for (PromTimeStamp t = start; t.isEarlierOrEquals(end); t = t.plusSeconds(step)) {
            //Handle missing data
            if (t.isEarlierThen(samples.get(i).getKey())) {
                if (!samplesBelongToInterval.isEmpty()) {
                    result.addAll(samplesBelongToInterval.stream()
                            .map(promTimeStamp -> Pair.of(promTimeStamp.getKey(), promTimeStamp.getValue().divide(BigDecimal.valueOf(samplesBelongToInterval.size()), BigDecimal.ROUND_HALF_EVEN, 2).stripTrailingZeros()))
                            .collect(Collectors.toList()));
                    samplesBelongToInterval.clear();
                    intervalBeginTime = t;
                }
                intervalBeginTime = intervalBeginTime.plusSeconds(interval).equals(t) ? t : intervalBeginTime;
                continue;
            }

            samplesBelongToInterval.add(samples.get(i));

            if (intervalBeginTime.plusSeconds(interval).equals(t) || i >= samples.size() - 1) {
                result.addAll(samplesBelongToInterval.stream()
                        .map(promTimeStamp -> Pair.of(promTimeStamp.getKey(), promTimeStamp.getValue().divide(BigDecimal.valueOf(samplesBelongToInterval.size()), BigDecimal.ROUND_HALF_EVEN, 2).stripTrailingZeros()))
                        .collect(Collectors.toList()));
                samplesBelongToInterval.clear();
                intervalBeginTime = t;
            }
            i++;

        }
        return result;
    }

    private static List<Pair<PromTimeStamp, BigDecimal>> makeContinuousAfterRestart(List<Pair<PromTimeStamp, BigDecimal>> samples,
                                                                                    int step,
                                                                                    PromTimeStamp start,
                                                                                    PromTimeStamp end) {
        int j = 0;
        BigDecimal previousValue = BigDecimal.ZERO;
        BigDecimal shiftedValue = BigDecimal.ZERO;
        // Handle pod restart
        boolean missingData = false;
        List<Pair<PromTimeStamp, BigDecimal>> samplesContinuous = new ArrayList<>();
        for (PromTimeStamp t = start; t.isEarlierOrEquals(end); t = t.plusSeconds(step)) {
            if (t.isEarlierThen(samples.get(j).getKey())) {
                missingData = true;
                continue;
            }
            Pair<PromTimeStamp, BigDecimal> sample = samples.get(j);
            if (previousValue.compareTo(sample.getValue()) > 0 || missingData) {
                shiftedValue = sample.getValue().add(previousValue);
                missingData = false;
            } else {
                shiftedValue = shiftedValue.add(sample.getValue()).subtract(previousValue);
            }
            samplesContinuous.add(Pair.of(sample.getKey(), shiftedValue));
            previousValue = sample.getValue();
            j++;
        }
        return samplesContinuous;
    }

}
