package codes.marcinnowak.prometheus.lab;

import codes.marcinnowak.prometheus.lab.calculation.PromTimeStamp;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;

public class Utility {
    public static Pair<PromTimeStamp, BigDecimal> sample(long time, String value) {
        return Pair.of(PromTimeStamp.of(BigDecimal.valueOf(time)), new BigDecimal(value));
    }
}
