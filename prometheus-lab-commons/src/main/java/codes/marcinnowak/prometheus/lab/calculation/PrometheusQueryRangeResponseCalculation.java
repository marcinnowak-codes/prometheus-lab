package codes.marcinnowak.prometheus.lab.calculation;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Singular;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.*;

@Data
@Builder
public class PrometheusQueryRangeResponseCalculation {
    String status;
    ResponseDataCalculation data;

    @Data
    @Builder(toBuilder = true)
    public static class ResponseDataCalculation {
        String resultType;
        @JsonProperty("result")
        @Singular
        List<ResponseResultCalculation> results;

        public static class ResponseResultCalculation {
            @JsonProperty("metric")
            Map<String, String> metrics;

            List<Pair<PromTimeStamp, BigDecimal>> values;

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;

                if (o == null || getClass() != o.getClass()) return false;

                ResponseResultCalculation that = (ResponseResultCalculation) o;

                return new EqualsBuilder()
                        .append(metrics, that.metrics)
                        .append(values, that.values)
                        .isEquals();
            }

            @Override
            public int hashCode() {
                return new HashCodeBuilder(17, 37)
                        .append(metrics)
                        .append(values)
                        .toHashCode();
            }

            @Override
            public String toString() {
                return new ToStringBuilder(this)
                        .append("metrics", metrics)
                        .append("values", values)
                        .toString();
            }

            public void setMetrics(Map<String, String> metricsMap) {
                this.metrics = new HashMap<>(metricsMap);
            }

            public ResponseResultCalculation(Map<String, String> metrics, List<Pair<PromTimeStamp, BigDecimal>> values) {

                this.metrics = metrics;
                this.values = values;
            }

            public static ResponseResultBuilder builder(){
                return new ResponseResultBuilder();
            }

            public Map<String, String> getMetrics() {
                return metrics;
            }

            public List<Pair<PromTimeStamp, BigDecimal>> getValues() {
                return values;
            }

            public void setValues(List<Pair<PromTimeStamp, BigDecimal>> samples) {
                values = new ArrayList<>(samples);
            }

            public static class ResponseResultBuilder{
                Map<String, String> metrics = new HashMap<>();

                List<Pair<PromTimeStamp, BigDecimal>> values = new ArrayList<>();

                public ResponseResultBuilder metric(String key, String value) {
                    metrics.put(key, value);
                    return this;
                }

                public ResponseResultBuilder value(Pair<PromTimeStamp, BigDecimal> samples) {
                    values.add(samples);
                    return this;
                }

                public ResponseResultBuilder value(PromTimeStamp timeStamp, BigDecimal value) {
                    this.values.add(Pair.of(timeStamp, value));
                    return this;
                }

                public ResponseResultBuilder metrics(Map<String, String> aMetrics) {
                    metrics.putAll(aMetrics);
                    return this;
                }

                public ResponseResultCalculation build() {
                    return new ResponseResultCalculation(metrics, values);
                }

                public ResponseResultBuilder values(List<Pair<PromTimeStamp, BigDecimal>> samples) {
                    values.addAll(samples);
                    return this;
                }
            }
        }

    }

}
