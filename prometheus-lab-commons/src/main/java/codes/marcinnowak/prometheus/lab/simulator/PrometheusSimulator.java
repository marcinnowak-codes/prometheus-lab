package codes.marcinnowak.prometheus.lab.simulator;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;

public interface PrometheusSimulator {
    PrometheusQueryRangeResponse queryRange(PrometheusQueryRequest promQueryRange);
}
