package codes.marcinnowak.prometheus.lab.simulator;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;

public class PrometheusSimulatorStatic implements PrometheusSimulator {

    @Override
    public PrometheusQueryRangeResponse queryRange(PrometheusQueryRequest promQueryRange) {
        return PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "1.0")
                                .value(1600000008, "1.0")
                                .value(1600000009, "1.0")
                                .value(1600000010, "1.0")
                                .value(1600000011, "1.0")  // interval
                                .value(1600000012, "2.0")
                                .value(1600000013, "2.0")
                                .value(1600000014, "2.0")
                                .value(1600000015, "2.0")
                                .value(1600000016, "2.0")  // interval
                                .value(1600000017, "3.0")
                                .value(1600000018, "3.0")
                                .value(1600000019, "3.0")
                                .value(1600000020, "3.0")
                                .value(1600000021, "3.0")  // interval
                                .value(1600000022, "4.0")
                                .value(1600000023, "4.0")
                                .value(1600000024, "4.0")
                                .value(1600000025, "4.0")
                                .value(1600000026, "4.0")  // interval
                                .value(1600000027, "5.0")
                                .value(1600000028, "5.0")
                                .value(1600000029, "5.0")
                                .value(1600000030, "5.0")
                                .value(1600000031, "5.0")  // interval
                                .value(1600000032, "6.0")
                                .value(1600000033, "6.0")
                                .value(1600000034, "6.0")
                                .value(1600000035, "6.0")
                                .value(1600000036, "6.0")  // interval
                                .value(1600000037, "6.0")
                                .value(1600000038, "6.0")
                                .value(1600000039, "6.0")
                                .value(1600000040, "6.0")  // interval
                                .build())
                        .build())
                .build();
    }
}
