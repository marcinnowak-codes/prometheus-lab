package codes.marcinnowak.prometheus.lab.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class PrometheusQueryRequest {
    private final String query;

    private final BigDecimal start;

    private final BigDecimal end;

    private final int step;

    private final long underline;
}
