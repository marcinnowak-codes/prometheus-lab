package codes.marcinnowak.prometheus.lab.simulator;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

class PrometheusSimulatorRandomInitialTest {

    @Test
    void shouldQueryRangeStep1() {
        // Given
        final PrometheusSimulatorRandomInitial prometheusSimulatorRandom
                = new PrometheusSimulatorRandomInitial(5, 1_234L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(5))
                .step(1)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "7")
                                .value(2L, "11")
                                .value(3L, "15")
                                .value(4L, "19")
                                .value(5L, "23")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeStep2() {
        // Given
        final PrometheusSimulatorRandomInitial prometheusSimulatorRandom
                = new PrometheusSimulatorRandomInitial(5, 1_234L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(9))
                .step(2)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "7")
                                .value(3L, "11")
                                .value(5L, "15")
                                .value(7L, "19")
                                .value(9L, "23")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeIncrease3() {
        // Given
        final PrometheusSimulatorRandomInitial prometheusSimulatorRandom
                = new PrometheusSimulatorRandomInitial(3, 1_234L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(5))
                .step(1)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "3")
                                .value(2L, "5")
                                .value(3L, "7")
                                .value(4L, "9")
                                .value(5L, "11")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeDifferentSeed() {
        // Given
        final PrometheusSimulatorRandomInitial prometheusSimulatorRandom =
                new PrometheusSimulatorRandomInitial(5, 12_321L);
        // When
        final PrometheusQueryRangeResponse prometheusQueryRangeResponse = prometheusSimulatorRandom.queryRange(PrometheusQueryRequest.builder()
                .query("http_requests")
                .start(BigDecimal.valueOf(1))
                .end(BigDecimal.valueOf(5))
                .step(1)
                .build());
        //Then
        assertThat(prometheusQueryRangeResponse).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(PrometheusQueryRangeResponse.ResponseData.builder()
                        .resultType("matrix")
                        .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1L, "2")
                                .value(2L, "3")
                                .value(3L, "4")
                                .value(4L, "5")
                                .value(5L, "6")
                                .build())
                        .build())
                .build());
    }

}
