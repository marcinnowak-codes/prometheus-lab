package codes.marcinnowak.prometheus.lab.promql;

import codes.marcinnowak.prometheus.lab.calculation.PromTimeStamp;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static codes.marcinnowak.prometheus.lab.promql.PromQlVisitorReversePolishNotationCalculation.increaseNormalize;
import static codes.marcinnowak.prometheus.lab.Utility.sample;
import static org.assertj.core.api.Assertions.assertThat;

@TestMethodOrder(MethodOrderer.Random.class)
class NormalizedIncreaseTest {

    @Test
    void shouldIncreaseStepEqualInterval_1() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),  // interval
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),  // interval
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"), // interval
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"), // interval
                sample(1600000009, "16"), // interval
                sample(1600000010, "16"), // interval
                sample(1600000011, "16")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(1), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        increase =  increaseNormalize(increase, BigDecimal.valueOf(1), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),  // interval
                sample(1600000003, "0"),  // interval
                sample(1600000004, "0"),  // interval
                sample(1600000005, "5"),  // interval
                sample(1600000006, "0"),  // interval
                sample(1600000007, "0"),  // interval
                sample(1600000008, "0"),  // interval
                sample(1600000009, "6"),  // interval
                sample(1600000010, "0"),  // interval
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseStepEqualInterval_2() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000003, "5"),  // interval
                sample(1600000005, "5"),  // interval
                sample(1600000007, "5"),  // interval
                sample(1600000009, "10"), // interval
                sample(1600000011, "10"), // interval
                sample(1600000013, "10"), // interval
                sample(1600000015, "10"), // interval
                sample(1600000017, "16"), // interval
                sample(1600000019, "16"), // interval
                sample(1600000021, "16")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 2, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000021"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 2, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000021"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000003, "0"),  // interval
                sample(1600000005, "0"),  // interval
                sample(1600000007, "0"),  // interval
                sample(1600000009, "5"),  // interval
                sample(1600000011, "0"),  // interval
                sample(1600000013, "0"),  // interval
                sample(1600000015, "0"),  // interval
                sample(1600000017, "6"),  // interval
                sample(1600000019, "0"),  // interval
                sample(1600000021, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseWithoutIntervalReminder() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "2.5"),
                sample(1600000005, "2.5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "2.5"),
                sample(1600000009, "2.5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseEndIntervalReminderNotWholeWithZeroDiff() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15"), // interval
                sample(1600000012, "15")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000012"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000012"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "2.5"),
                sample(1600000005, "2.5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "2.5"),
                sample(1600000009, "2.5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0"),  // interval
                sample(1600000012, "0")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseEndIntervalReminderNotWholeWithNotZeroDiff() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15"), // interval
                sample(1600000012, "20")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000012"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000012"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "2.5"),
                sample(1600000005, "2.5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "2.5"),
                sample(1600000009, "2.5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0"),  // interval
                sample(1600000012, "5")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldIncreaseIntervalOdd5() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "15"), // interval
                sample(1600000002, "15"),
                sample(1600000003, "15"),
                sample(1600000004, "15"),
                sample(1600000005, "15"),
                sample(1600000006, "15"), // interval
                sample(1600000007, "15"),
                sample(1600000008, "15"),
                sample(1600000009, "15"),
                sample(1600000010, "20"),
                sample(1600000011, "20"), // interval
                sample(1600000012, "20"),
                sample(1600000013, "20"),
                sample(1600000014, "20"),
                sample(1600000015, "20"),
                sample(1600000016, "20"), // interval
                sample(1600000017, "20"),
                sample(1600000018, "20"),
                sample(1600000019, "20"),
                sample(1600000020, "25"),
                sample(1600000021, "25"), // interval
                sample(1600000022, "25"),
                sample(1600000023, "25"),
                sample(1600000024, "25"),
                sample(1600000025, "25"),
                sample(1600000026, "25"), // interval
                sample(1600000027, "25"),
                sample(1600000028, "25"),
                sample(1600000029, "25"),
                sample(1600000030, "30"),
                sample(1600000031, "30"), // interval
                sample(1600000032, "30"),
                sample(1600000033, "30"),
                sample(1600000034, "30"),
                sample(1600000035, "30"),
                sample(1600000036, "30"), // interval
                sample(1600000037, "30"),
                sample(1600000038, "30"),
                sample(1600000039, "30"),
                sample(1600000040, "30")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(5), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(5), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"), // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),
                sample(1600000004, "0"),
                sample(1600000005, "0"),
                sample(1600000006, "0"), // interval
                sample(1600000007, "1"),
                sample(1600000008, "1"),
                sample(1600000009, "1"),
                sample(1600000010, "1"),
                sample(1600000011, "1"), // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),
                sample(1600000014, "0"),
                sample(1600000015, "0"),
                sample(1600000016, "0"), // interval
                sample(1600000017, "1"),
                sample(1600000018, "1"),
                sample(1600000019, "1"),
                sample(1600000020, "1"),
                sample(1600000021, "1"), // interval
                sample(1600000022, "0"),
                sample(1600000023, "0"),
                sample(1600000024, "0"),
                sample(1600000025, "0"),
                sample(1600000026, "0"), // interval
                sample(1600000027, "1"),
                sample(1600000028, "1"),
                sample(1600000029, "1"),
                sample(1600000030, "1"),
                sample(1600000031, "1"), // interval
                sample(1600000032, "0"),
                sample(1600000033, "0"),
                sample(1600000034, "0"),
                sample(1600000035, "0"),
                sample(1600000036, "0"), // interval
                sample(1600000037, "0"),
                sample(1600000038, "0"),
                sample(1600000039, "0"),
                sample(1600000040, "0")  // interval
        );

        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleRestartToCounterEqualToZero() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "0"),  // <- Restart
                sample(1600000007, "0"),  // interval
                sample(1600000008, "0"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "5"),
                sample(1600000011, "5")   // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "2.5"),
                sample(1600000005, "2.5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "2.5"),
                sample(1600000009, "2.5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleRestartToCounterGraterThenZero() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "2"),  // <- Restart
                sample(1600000007, "2"),  // interval
                sample(1600000008, "2"),
                sample(1600000009, "5"),  // interval
                sample(1600000010, "5"),
                sample(1600000011, "5")   // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "2.5"),
                sample(1600000005, "2.5"),  // interval
                sample(1600000006, "1"),
                sample(1600000007, "1"),  // interval
                sample(1600000008, "1.5"),
                sample(1600000009, "1.5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingBeginning1sample() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
//                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                sample(1600000005, "10"), // interval
                sample(1600000006, "10"),
                sample(1600000007, "10"), // interval
                sample(1600000008, "10"),
                sample(1600000009, "15"), // interval
                sample(1600000010, "15"),
                sample(1600000011, "15")  // interval
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000011"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
//                sample(1600000001, "0"),  // interval
                sample(1600000002, "2.5"),
                sample(1600000003, "2.5"),  // interval
                sample(1600000004, "2.5"),
                sample(1600000005, "2.5"),  // interval
                sample(1600000006, "0"),
                sample(1600000007, "0"),  // interval
                sample(1600000008, "2.5"),
                sample(1600000009, "2.5"),  // interval
                sample(1600000010, "0"),
                sample(1600000011, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingDataAfterMissingHigherThenBefore() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "10"),
                sample(1600000011, "10"),  // interval
                sample(1600000012, "10"),
                sample(1600000013, "10"),  // interval
                sample(1600000014, "15"),
                sample(1600000015, "15"), // interval
                sample(1600000016, "15")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000016"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000016"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "0"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "5"),
                sample(1600000011, "5"),  // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),  // interval
                sample(1600000014, "2.5"),
                sample(1600000015, "2.5"),  // interval
                sample(1600000016, "0")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingDataAfterMissingLowerThenBefore() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "5"),  // interval
                sample(1600000002, "5"),
                sample(1600000003, "5"),  // interval
                sample(1600000004, "5"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "2"),
                sample(1600000011, "2"),  // interval
                sample(1600000012, "2"),
                sample(1600000013, "2"),  // interval
                sample(1600000014, "15"),
                sample(1600000015, "15"), // interval
                sample(1600000016, "15")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000016"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(2), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000016"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "0"),  // interval
                sample(1600000002, "0"),
                sample(1600000003, "0"),  // interval
                sample(1600000004, "0"),
                //                    5                  interval
                //                    6
                //                    7                  interval
                //                    8
                //                    9                  interval
                sample(1600000010, "1"),
                sample(1600000011, "1"),  // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),  // interval
                sample(1600000014, "6.5"),
                sample(1600000015, "6.5"),  // interval
                sample(1600000016, "0")
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void shouldHandleMissingDataInterval5s() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000008, "15"),
                sample(1600000009, "20"),
                sample(1600000010, "20"),
                sample(1600000011, "20"),
                sample(1600000012, "20"),
                sample(1600000013, "20"),
                sample(1600000014, "20"),
                sample(1600000015, "20"),
                sample(1600000016, "20"),
                sample(1600000017, "20"),
                sample(1600000018, "20"),
                sample(1600000019, "20"),
                sample(1600000020, "20"),
                sample(1600000021, "20"),
                sample(1600000022, "20"),
                sample(1600000023, "20"),
                sample(1600000024, "20"),
                sample(1600000025, "20"),
                sample(1600000026, "20"),
                sample(1600000027, "20"),
                sample(1600000028, "20"),
                sample(1600000029, "20"),
                sample(1600000030, "20"),
                sample(1600000031, "20"),
                sample(1600000032, "20"),
                sample(1600000033, "20"),
                sample(1600000034, "20"),
                sample(1600000035, "20"),
                sample(1600000036, "20"),
                sample(1600000037, "20"),
                sample(1600000038, "20"),
                sample(1600000039, "20"),
                sample(1600000040, "20")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(5), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));
        increase =  increaseNormalize(increase,
                BigDecimal.valueOf(5), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
//                           1600000001,              // interval
//                           1600000002,
//                           1600000003,
//                           1600000004,
//                           1600000005,
//                           1600000006,              // interval
//                           1600000007,
                sample(1600000008, "5"),
                sample(1600000009, "5"),
                sample(1600000010, "5"),
                sample(1600000011, "5"),  // interval
                sample(1600000012, "0"),
                sample(1600000013, "0"),
                sample(1600000014, "0"),
                sample(1600000015, "0"),
                sample(1600000016, "0"),  // interval
                sample(1600000017, "0"),
                sample(1600000018, "0"),
                sample(1600000019, "0"),
                sample(1600000020, "0"),
                sample(1600000021, "0"),  // interval
                sample(1600000022, "0"),
                sample(1600000023, "0"),
                sample(1600000024, "0"),
                sample(1600000025, "0"),
                sample(1600000026, "0"),  // interval
                sample(1600000027, "0"),
                sample(1600000028, "0"),
                sample(1600000029, "0"),
                sample(1600000030, "0"),
                sample(1600000031, "0"),  // interval
                sample(1600000032, "0"),
                sample(1600000033, "0"),
                sample(1600000034, "0"),
                sample(1600000035, "0"),
                sample(1600000036, "0"),  // interval
                sample(1600000037, "0"),
                sample(1600000038, "0"),
                sample(1600000039, "0"),
                sample(1600000040, "0")   // interval
        );
        assertThat(increase).isEqualTo(expected);
    }

    @Test
    void extractSubgroups() {
        // Given
        PrometheusQueryRangeResponse.ResponseData.ResponseResult responseResult1 = PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val1")
                .metric("status", "OK")
                .value(Arrays.asList(1600000001, "1"))  // interval
                .value(Arrays.asList(1600000002, "2"))
                .value(Arrays.asList(1600000003, "3"))
                .build();

        PrometheusQueryRangeResponse.ResponseData.ResponseResult responseResult2 = PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val1")
                .metric("status", "NOK")
                .value(Arrays.asList(1600000001, "4"))  // interval
                .value(Arrays.asList(1600000002, "5"))
                .value(Arrays.asList(1600000003, "6"))
                .build();

        PrometheusQueryRangeResponse.ResponseData.ResponseResult responseResult3 = PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val2")
                .metric("status", "OK")
                .value(Arrays.asList(1600000001, "7"))  // interval
                .value(Arrays.asList(1600000002, "8"))
                .value(Arrays.asList(1600000003, "9"))
                .build();

        PrometheusQueryRangeResponse.ResponseData.ResponseResult responseResult4 = PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                .metric("__name__", "some_counter_metric")
                .metric("instance", "some_instance")
                .metric("job", "some_job")
                .metric("label", "val2")
                .metric("status", "OK")
                .value(Arrays.asList(1600000001, "10"))  // interval
                .value(Arrays.asList(1600000002, "11"))
                .value(Arrays.asList(1600000003, "12"))
                .build();
        PrometheusQueryRangeResponse.ResponseData responseData = PrometheusQueryRangeResponse.ResponseData.builder()
                .result(responseResult1)
                .result(responseResult2)
                .result(responseResult3)
                .result(responseResult4)
                .build();

        // When
        List<PrometheusQueryRangeResponse.ResponseData> extractSubgroups = PQLParser.extractSubgroups(responseData, Arrays.asList("label"));

        // Then
        ArrayList<PrometheusQueryRangeResponse.ResponseData> expected = new ArrayList<>();

        expected.add(PrometheusQueryRangeResponse.ResponseData.builder()
                .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                        .metric("label", "val1")
                        .values(responseResult1.getValues())
                        .build())
                .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                        .metric("label", "val1")
                        .values(responseResult2.getValues())
                        .build())
                .build());

        expected.add(PrometheusQueryRangeResponse.ResponseData.builder()
                .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                        .metric("label", "val2")
                        .values(responseResult3.getValues())
                        .build())
                .result(PrometheusQueryRangeResponse.ResponseData.ResponseResult.builder()
                        .metric("label", "val2")
                        .values(responseResult4.getValues())
                        .build())
                .build());
        assertThat(extractSubgroups).isEqualTo(expected);
    }
}
