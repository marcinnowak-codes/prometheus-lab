package codes.marcinnowak.prometheus.lab.promql;

import codes.marcinnowak.prometheus.lab.calculation.PromTimeStamp;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static codes.marcinnowak.prometheus.lab.Utility.sample;
import static org.assertj.core.api.Assertions.assertThat;

class IncreasePeekBeforeMissingDataTest {
    @Test
    void shouldHandleMissingDataInterval10sPeekBeforeMissing_1() {
        // Given
        List<Pair<PromTimeStamp, BigDecimal>> samples = Arrays.asList(
                sample(1600000001, "15"),  // interval
                sample(1600000002, "15"),
                sample(1600000003, "15"),
                sample(1600000004, "15"),
                sample(1600000005, "15"),
                sample(1600000006, "15"),
                sample(1600000007, "15"),
                sample(1600000008, "15"),
                sample(1600000009, "20"),
//                sample(1600000010, "20"),
//                sample(1600000011, "20"),  // interval
//                sample(1600000012, "20"),
//                sample(1600000013, "20"),
                sample(1600000014, "20"),
                sample(1600000015, "20"),
                sample(1600000016, "20"),
                sample(1600000017, "20"),
                sample(1600000018, "20"),
                sample(1600000019, "20"),
                sample(1600000020, "20"),
                sample(1600000021, "20"),  // interval
                sample(1600000022, "20"),
                sample(1600000023, "20"),
                sample(1600000024, "20"),
                sample(1600000025, "20"),
                sample(1600000026, "20"),
                sample(1600000027, "20"),
                sample(1600000028, "20"),
                sample(1600000029, "20"),
                sample(1600000030, "20"),
                sample(1600000031, "20"),  // interval
                sample(1600000032, "20"),
                sample(1600000033, "20"),
                sample(1600000034, "20"),
                sample(1600000035, "20"),
                sample(1600000036, "20"),
                sample(1600000037, "20"),
                sample(1600000038, "20"),
                sample(1600000039, "20"),
                sample(1600000040, "20")
        );

        // When
        List<Pair<PromTimeStamp, BigDecimal>> increase = PromQlVisitorReversePolishNotationCalculation.increase(samples,
                BigDecimal.valueOf(10), 1, PromTimeStamp.of("1600000001"), PromTimeStamp.of("1600000040"));

        // Then
        List<Pair<PromTimeStamp, BigDecimal>> expected = Arrays.asList(
                sample(1600000001, "5"),
                sample(1600000002, "5"),
                sample(1600000003, "5"),
                sample(1600000004, "5"),
                sample(1600000005, "5"),
                sample(1600000006, "5"),
                sample(1600000007, "5"),
                sample(1600000008, "5"),
                sample(1600000009, "5"),
//                sample(1600000010, "5"),
//                sample(1600000011, "5"),  // interval
//                sample(1600000012, "0"),
//                sample(1600000013, "0"),
                sample(1600000014, "20"),
                sample(1600000015, "20"),
                sample(1600000016, "20"),
                sample(1600000017, "20"),
                sample(1600000018, "20"),
                sample(1600000019, "20"),
                sample(1600000020, "20"),
                sample(1600000021, "0"),  // interval
                sample(1600000022, "0"),
                sample(1600000023, "0"),
                sample(1600000024, "0"),
                sample(1600000025, "0"),
                sample(1600000026, "0"),
                sample(1600000027, "0"),
                sample(1600000028, "0"),
                sample(1600000029, "0"),
                sample(1600000030, "0"),
                sample(1600000031, "0"),  // interval
                sample(1600000032, "0"),
                sample(1600000033, "0"),
                sample(1600000034, "0"),
                sample(1600000035, "0"),
                sample(1600000036, "0"),
                sample(1600000037, "0"),
                sample(1600000038, "0"),
                sample(1600000039, "0"),
                sample(1600000040, "0")
        );
        assertThat(increase).isEqualTo(expected);
    }
}
