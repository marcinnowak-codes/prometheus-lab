package codes.marcinnowak.disturber;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestOperations;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Enumeration;

@Controller
public class DisturberController {

    private final Properties properties;

    private final RestOperations restTemplate;

    public DisturberController(Properties properties,
                               @Qualifier("DisturbedRestOperations") RestOperations restTemplate) {
        this.properties = properties;
        this.restTemplate = restTemplate;
    }

    @RequestMapping("/**")
    public ResponseEntity disturb(@RequestBody(required = false) String body,
                                     HttpMethod method,
                                     HttpServletRequest request,
                                     HttpServletResponse response) throws URISyntaxException {

        String requestUrl = request.getRequestURI();

        URI uri = new URI(properties.getBaseUrl());
        uri = UriComponentsBuilder.fromUri(uri)
                .path(requestUrl)
                .query(request.getQueryString())
                .build(true).toUri();

        HttpHeaders headers = new HttpHeaders();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            if ("Accept-Encoding".equals(headerName))
                continue;
            headers.set(headerName, request.getHeader(headerName));
        }

        HttpEntity<String> httpEntity = new HttpEntity<>(body, headers);

        try {
            return restTemplate.exchange(uri, method, httpEntity, String.class);
        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getRawStatusCode())
                    .headers(e.getResponseHeaders())
                    .body(e.getResponseBodyAsString());
        }
    }

}
