package codes.marcinnowak.disturber;

import codes.marcinnowak.disturber.client.RestOperationsExceptionDisturber;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import java.util.function.Supplier;

import static org.springframework.http.HttpStatus.*;

@Slf4j
@Configuration
public class BeanFactory {
    private final org.springframework.boot.web.client.RestTemplateBuilder builder;
    private final Properties properties;
    private final MeterRegistry meterRegistry;

    public BeanFactory(org.springframework.boot.web.client.RestTemplateBuilder builder, Properties properties, MeterRegistry meterRegistry) {
        this.builder = builder;
        this.properties = properties;
        this.meterRegistry = meterRegistry;
    }

    @Bean(name = "DisturbedRestOperations")
    public RestOperations createPrometheus() {
        return new DisturbedRestTemplateBuilder(builder.build(), meterRegistry)
                .withExceptionDisturber(
                        properties.getRestClientException().getEnable(),
                        properties.getRestClientException().getProbability(),
                        () -> new RestClientException("Disturber exception trowed"),
                        "rest-client")
                .withExceptionDisturber(
                        properties.getBadRequest().getEnable(),
                        properties.getBadRequest().getProbability(),
                        () -> HttpServerErrorException.create(BAD_REQUEST, "Bad Request", HttpHeaders.EMPTY, null, null),
                        "bad-request")
                .withExceptionDisturber(
                        properties.getInternalServerError().getEnable(),
                        properties.getInternalServerError().getProbability(),
                        () -> HttpServerErrorException.create(INTERNAL_SERVER_ERROR, "Internal Server Error", HttpHeaders.EMPTY, null, null),
                        "internal-server-error")
                .withExceptionDisturber(
                        properties.getNotImplemented().getEnable(),
                        properties.getNotImplemented().getProbability(),
                        () -> HttpServerErrorException.create(NOT_IMPLEMENTED, "Not Implemented", HttpHeaders.EMPTY, null, null),
                        "not-implemented")
                .withExceptionDisturber(
                        properties.getBadGateway().getEnable(),
                        properties.getBadGateway().getProbability(),
                        () -> HttpServerErrorException.create(BAD_GATEWAY, "Bad Gateway", HttpHeaders.EMPTY, null, null),
                        "bad-gateway")
                .build();
    }

    private static class DisturbedRestTemplateBuilder {

        private RestOperations restOperations;
        private final MeterRegistry meterRegistry;

        public DisturbedRestTemplateBuilder(RestOperations restOperations, MeterRegistry meterRegistry) {

            this.restOperations = restOperations;
            this.meterRegistry = meterRegistry;
        }

        public DisturbedRestTemplateBuilder withExceptionDisturber(boolean isEnabled,
                                                                   Double probability,
                                                                   Supplier<RestClientException> exceptionSupplier,
                                                                   String metricName) {
            if (isEnabled) {
                restOperations = new RestOperationsExceptionDisturber(restOperations,
                        probability,
                        exceptionSupplier,
                        Counter.builder("exception_disturber")
                                .description("indicates statistics for disturber")
                                .tags("kind", metricName + "-exception")
                                .register(meterRegistry),
                        Counter.builder("exception_disturber")
                                .description("indicates statistics for disturber")
                                .tags("kind", metricName + "-pass")
                                .register(meterRegistry));

            }
            return this;
        }

        public RestOperations build() {
            return restOperations;

        }

    }
}
