package codes.marcinnowak.disturber;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;

@Data
@Validated
@Component
@ConfigurationProperties("disturber-http")
public class Properties {
    private String baseUrl;

    private DisturberProperties restClientException;
    private DisturberProperties badRequest;
    private DisturberProperties internalServerError;
    private DisturberProperties notImplemented;
    private DisturberProperties badGateway;

    @Data
    public static class DisturberProperties {
        @NotNull
        private Boolean enable;

        @NotNull
        private Double probability;
    }
}
