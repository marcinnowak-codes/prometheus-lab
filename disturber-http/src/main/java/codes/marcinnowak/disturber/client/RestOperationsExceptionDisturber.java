package codes.marcinnowak.disturber.client;

import io.micrometer.core.instrument.Counter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import javax.validation.constraints.NotEmpty;
import java.net.URI;
import java.util.function.Supplier;

@Slf4j
public class RestOperationsExceptionDisturber extends RestOperationsDecorator {
    private final Double probability;
    private final Supplier<? extends RestClientException> exceptionSupplier;
    private final Counter exceptionCounter;
    private final Counter passThroughCounter;

    public RestOperationsExceptionDisturber(RestOperations decorated,
                                            @NotEmpty Double probability,
                                            Supplier<? extends RestClientException> exceptionSupplier,
                                            Counter exceptionCounter,
                                            Counter passThroughCounter) {
        super(decorated);
        this.probability = probability;
        this.exceptionSupplier = exceptionSupplier;
        this.exceptionCounter = exceptionCounter;
        this.passThroughCounter = passThroughCounter;
        log.info("Probability: {}", probability);
    }

    private <T> T shouldTrigger(Supplier<T> supplier) {
        if(Math.random() < probability) {
            exceptionCounter.increment();
            throw exceptionSupplier.get();
        }
        passThroughCounter.increment();
        return supplier.get();
    }

    @Override
    public <T> T getForObject(URI url, Class<T> responseType) throws RestClientException {
        return shouldTrigger(()->super.getForObject(url, responseType));
    }

    @Override
    public <T> ResponseEntity<T> exchange(URI url, HttpMethod method, @Nullable HttpEntity<?> requestEntity,
                                          Class<T> responseType) throws RestClientException{
        return shouldTrigger(()->super.exchange(url, method, requestEntity, responseType));
    }

}
