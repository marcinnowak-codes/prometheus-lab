package codes.marcinnowak.prometheus.lab.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PrometheusLabApplication {

    public static void main(String[] args) {
        SpringApplication.run(PrometheusLabApplication.class, args);
    }

}
