package codes.marcinnowak.prometheus.lab.web.factory;

import codes.marcinnowak.prometheus.lab.web.client.RestOperationsRetry;
import codes.marcinnowak.prometheus.lab.web.config.Properties;
import codes.marcinnowak.prometheus.lab.web.simulator.PrometheusRestSimulator;
import codes.marcinnowak.prometheus.lab.web.service.PrometheusService;
import codes.marcinnowak.prometheus.lab.web.service.PrometheusServiceProxy;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestOperations;

@Slf4j
@Configuration
public class BeanFactory {
    private final RestTemplateBuilder builder;
    private final Properties properties;
    private final MeterRegistry meterRegistry;

    public BeanFactory(RestTemplateBuilder builder, Properties properties, MeterRegistry meterRegistry) {
        this.builder = builder;
        this.properties = properties;
        this.meterRegistry = meterRegistry;
        log.info("Simulator is {}", properties.getSimulator().getEnable());
    }

    @Bean
    public PrometheusService createPrometheus() {
        return new PrometheusServiceBuilder(builder.build(), meterRegistry)
                .withSimulator(properties.getSimulator().getEnable(), Counter.builder("simulator")
                        .description("indicates statistics for simulator")
                        .tags("kind", "query-range")
                        .register(meterRegistry))
                .withPrometheusService(properties.getPrometheus().getBaseUrl())
                .withRetry(properties.getRetry().getEnable(),
                        properties.getRetry().getMaxAttempts(),
                        meterRegistry)
                .build();
    }

    private static class PrometheusServiceBuilder {

        private RestOperations restOperations;
        private String prometheusBaseUrl;

        public PrometheusServiceBuilder(RestOperations restOperations, MeterRegistry meterRegistry) {

            this.restOperations = restOperations;
        }

        public PrometheusServiceBuilder withPrometheusService(String baseUrl) {
            this.prometheusBaseUrl = baseUrl;
            return this;
        }

        public PrometheusService build() {
            return new PrometheusServiceProxy(restOperations, prometheusBaseUrl);

        }

        public PrometheusServiceBuilder withRetry(boolean isRetryEnabled,
                                                  Integer maxAttempts,
                                                  MeterRegistry meterRegistry) {
            if (isRetryEnabled) {
                restOperations = new RestOperationsRetry(restOperations, maxAttempts, meterRegistry);
            }
            return this;
        }

        public PrometheusServiceBuilder withSimulator(Boolean isSimulatorEnable, Counter counter) {
            restOperations = isSimulatorEnable ? new PrometheusRestSimulator(counter) : restOperations;
            return this;
        }

    }
}
