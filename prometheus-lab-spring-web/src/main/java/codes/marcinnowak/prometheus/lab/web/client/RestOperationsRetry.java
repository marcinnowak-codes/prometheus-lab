package codes.marcinnowak.prometheus.lab.web.client;

import io.github.resilience4j.micrometer.tagged.TaggedRetryMetrics;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryConfig;
import io.github.resilience4j.retry.RetryRegistry;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

import java.net.URI;
import java.time.Duration;
import java.util.function.Supplier;

import static java.time.temporal.ChronoUnit.SECONDS;

public class RestOperationsRetry extends RestOperationsDecorator {

    private final RetryConfig config;
    private final RetryRegistry retryRegistry;

    public RestOperationsRetry(RestOperations decorated,
                               int maxAttempts,
                               MeterRegistry meterRegistry) {
        super(decorated);
        config = RetryConfig.custom()
                .maxAttempts(maxAttempts)
                .waitDuration(Duration.of(3, SECONDS))
                .retryExceptions(RestClientException.class)
                .build();

        retryRegistry = RetryRegistry.of(config);

        TaggedRetryMetrics.ofRetryRegistry(retryRegistry)
                .bindTo(meterRegistry);

    }

    @Override
    public <T> T getForObject(URI url, Class<T> responseType) throws RestClientException {

        Retry retry = retryRegistry.retry("rest_operation_retry", config);
        Supplier<T> decorated = Retry.decorateSupplier(retry, () -> super.getForObject(url, responseType));

        return decorated.get();
    }

}
