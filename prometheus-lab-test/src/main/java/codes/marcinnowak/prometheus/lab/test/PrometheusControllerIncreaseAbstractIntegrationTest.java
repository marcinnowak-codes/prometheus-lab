package codes.marcinnowak.prometheus.lab.test;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse.ResponseData;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse.ResponseData.ResponseResult;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.web.util.UriComponentsBuilder;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWireMock(stubs = "classpath:/stubs/mappings", port = 0)
public abstract class PrometheusControllerIncreaseAbstractIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void shouldQueryRangeIncrease() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_increase(some_counter_metric[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "5.0")
                                .value(1600000008, "5.0")
                                .value(1600000009, "5.0")
                                .value(1600000010, "5.0")
                                .value(1600000011, "5.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "5.0")
                                .value(1600000028, "5.0")
                                .value(1600000029, "5.0")
                                .value(1600000030, "5.0")
                                .value(1600000031, "5.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldQueryRangeIncreaseWithMultiplication() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_increase(some_counter_metric[2*5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "some_counter_metric")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "5.0")  // interval
                                .value(1600000002, "5.0")
                                .value(1600000003, "5.0")
                                .value(1600000004, "5.0")
                                .value(1600000005, "5.0")
                                .value(1600000006, "5.0")
                                .value(1600000007, "5.0")
                                .value(1600000008, "5.0")
                                .value(1600000009, "5.0")
                                .value(1600000010, "5.0")
                                .value(1600000011, "5.0")  // interval
                                .value(1600000012, "5.0")
                                .value(1600000013, "5.0")
                                .value(1600000014, "5.0")
                                .value(1600000015, "5.0")
                                .value(1600000016, "5.0")
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "5.0")
                                .value(1600000023, "5.0")
                                .value(1600000024, "5.0")
                                .value(1600000025, "5.0")
                                .value(1600000026, "5.0")
                                .value(1600000027, "5.0")
                                .value(1600000028, "5.0")
                                .value(1600000029, "5.0")
                                .value(1600000030, "5.0")
                                .value(1600000031, "5.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandel2Labels() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_increase(some_counter_with_label[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "some_counter_with_label")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "5.0")
                                .value(1600000008, "5.0")
                                .value(1600000009, "5.0")
                                .value(1600000010, "5.0")
                                .value(1600000011, "5.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "5.0")
                                .value(1600000028, "5.0")
                                .value(1600000029, "5.0")
                                .value(1600000030, "5.0")
                                .value(1600000031, "5.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .result(ResponseResult.builder()
                                .metric("__name__", "some_counter_with_label")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val2")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "10.0")
                                .value(1600000008, "10.0")
                                .value(1600000009, "10.0")
                                .value(1600000010, "10.0")
                                .value(1600000011, "10.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "6.0")
                                .value(1600000028, "6.0")
                                .value(1600000029, "6.0")
                                .value(1600000030, "6.0")
                                .value(1600000031, "6.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelSum() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_increase(some_counter_with_label[5s]))")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "some_counter_with_label")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("label", "val1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "15.0")
                                .value(1600000008, "15.0")
                                .value(1600000009, "15.0")
                                .value(1600000010, "15.0")
                                .value(1600000011, "15.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "10.0")
                                .value(1600000018, "10.0")
                                .value(1600000019, "10.0")
                                .value(1600000020, "10.0")
                                .value(1600000021, "10.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "11.0")
                                .value(1600000028, "11.0")
                                .value(1600000029, "11.0")
                                .value(1600000030, "11.0")
                                .value(1600000031, "11.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelSumBy() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_increase(counter_with_label_ip_status[5s])) by (ip)")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("ip", "192.16.0.1")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "15.0")
                                .value(1600000008, "15.0")
                                .value(1600000009, "15.0")
                                .value(1600000010, "15.0")
                                .value(1600000011, "15.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "10.0")
                                .value(1600000018, "10.0")
                                .value(1600000019, "10.0")
                                .value(1600000020, "10.0")
                                .value(1600000021, "10.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "11.0")
                                .value(1600000028, "11.0")
                                .value(1600000029, "11.0")
                                .value(1600000030, "11.0")
                                .value(1600000031, "11.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .result(ResponseResult.builder()
                                .metric("ip", "192.16.0.2")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0") // interval
                                .value(1600000007, "30.0")
                                .value(1600000008, "30.0")
                                .value(1600000009, "30.0")
                                .value(1600000010, "30.0")
                                .value(1600000011, "30.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0") // interval
                                .value(1600000017, "15.0")
                                .value(1600000018, "15.0")
                                .value(1600000019, "15.0")
                                .value(1600000020, "15.0")
                                .value(1600000021, "15.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "18.0")
                                .value(1600000028, "18.0")
                                .value(1600000029, "18.0")
                                .value(1600000030, "18.0")
                                .value(1600000031, "18.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelFilter() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_increase(counter_with_label_ip_status{ip=\"localhost\"}[5s])")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "200 (OK)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "5.0")
                                .value(1600000008, "5.0")
                                .value(1600000009, "5.0")
                                .value(1600000010, "5.0")
                                .value(1600000011, "5.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "5.0")
                                .value(1600000028, "5.0")
                                .value(1600000029, "5.0")
                                .value(1600000030, "5.0")
                                .value(1600000031, "5.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .result(ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "400 (Bad Request)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "10.0")
                                .value(1600000008, "10.0")
                                .value(1600000009, "10.0")
                                .value(1600000010, "10.0")
                                .value(1600000011, "10.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "5.0")
                                .value(1600000018, "5.0")
                                .value(1600000019, "5.0")
                                .value(1600000020, "5.0")
                                .value(1600000021, "5.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "6.0")
                                .value(1600000028, "6.0")
                                .value(1600000029, "6.0")
                                .value(1600000030, "6.0")
                                .value(1600000031, "6.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelSumFilterInterruptedFirstMetric() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_increase(counter_with_label_ip_status_interrupted_first_begin{ip=\"localhost\"}[5s]))")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status_interrupted_first_begin")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "200 (OK)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "0.0")
                                .value(1600000008, "20.0")
                                .value(1600000009, "20.0")
                                .value(1600000010, "20.0")
                                .value(1600000011, "20.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "0.0")
                                .value(1600000018, "0.0")
                                .value(1600000019, "0.0")
                                .value(1600000020, "0.0")
                                .value(1600000021, "0.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "0.0")
                                .value(1600000028, "0.0")
                                .value(1600000029, "0.0")
                                .value(1600000030, "0.0")
                                .value(1600000031, "0.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelFilterInterruptedSecondMetric() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_increase(counter_with_label_ip_status_interrupted_second_begin{ip=\"localhost\"}[5s]))")
                        .queryParam("start", "1600000001")
                        .queryParam("end", "1600000040")
                        .queryParam("step", "1")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "counter_with_label_ip_status_interrupted_second_begin")
                                .metric("instance", "some_instance")
                                .metric("job", "some_job")
                                .metric("ip", "localhost")
                                .metric("status", "200 (OK)")
                                .value(1600000001, "0.0")  // interval
                                .value(1600000002, "0.0")
                                .value(1600000003, "0.0")
                                .value(1600000004, "0.0")
                                .value(1600000005, "0.0")
                                .value(1600000006, "0.0")  // interval
                                .value(1600000007, "0.0")
                                .value(1600000008, "22.0")
                                .value(1600000009, "22.0")
                                .value(1600000010, "22.0")
                                .value(1600000011, "22.0")  // interval
                                .value(1600000012, "0.0")
                                .value(1600000013, "0.0")
                                .value(1600000014, "0.0")
                                .value(1600000015, "0.0")
                                .value(1600000016, "0.0")  // interval
                                .value(1600000017, "0.0")
                                .value(1600000018, "0.0")
                                .value(1600000019, "0.0")
                                .value(1600000020, "0.0")
                                .value(1600000021, "0.0")  // interval
                                .value(1600000022, "0.0")
                                .value(1600000023, "0.0")
                                .value(1600000024, "0.0")
                                .value(1600000025, "0.0")
                                .value(1600000026, "0.0")  // interval
                                .value(1600000027, "0.0")
                                .value(1600000028, "0.0")
                                .value(1600000029, "0.0")
                                .value(1600000030, "0.0")
                                .value(1600000031, "0.0")  // interval
                                .value(1600000032, "0.0")
                                .value(1600000033, "0.0")
                                .value(1600000034, "0.0")
                                .value(1600000035, "0.0")
                                .value(1600000036, "0.0")  // interval
                                .value(1600000037, "0.0")
                                .value(1600000038, "0.0")
                                .value(1600000039, "0.0")
                                .value(1600000040, "0.0")  // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelCounterWithCalculationFluctuationDuringTimeShift() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_increase(some_counter_metric[60s])) by (country)")
                        .queryParam("start", "1595880300")
                        .queryParam("end", "1595880660")
                        .queryParam("step", "15")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("country", "Germany")
                                .value(1595880300, "0.0")      // interval
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "0.0")
                                .value(1595880465, "0.0")
                                .value(1595880480, "0.0")
                                .value(1595880495, "5.0")      // interval
                                .value(1595880510, "5.0")
                                .value(1595880525, "5.0")
                                .value(1595880540, "5.0")
                                .value(1595880555, "0.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .build())
                        .result(ResponseResult.builder()
                                .metric("country", "Poland")
                                .value(1595880300, "0.0")      // interval
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "0.0")
                                .value(1595880465, "0.0")
                                .value(1595880480, "0.0")
                                .value(1595880495, "5.0")      // interval
                                .value(1595880510, "5.0")
                                .value(1595880525, "5.0")
                                .value(1595880540, "5.0")
                                .value(1595880555, "0.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelCounterWithCalculationFluctuationDuringTimeShiftShifted() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "sum(lab_increase(some_counter_metric[60s])) by (country)")
                        .queryParam("start", "1595880315")
                        .queryParam("end", "1595880675")
                        .queryParam("step", "15")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("country", "Germany")
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "5.0")
                                .value(1595880465, "5.0")
                                .value(1595880480, "5.0")
                                .value(1595880495, "5.0")      // interval
                                .value(1595880510, "0.0")
                                .value(1595880525, "0.0")
                                .value(1595880540, "0.0")
                                .value(1595880555, "0.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .value(1595880675, "0.0")      // interval
                                .build())
                        .result(ResponseResult.builder()
                                .metric("country", "Poland")
                                .value(1595880315, "0.0")      // interval
                                .value(1595880330, "0.0")
                                .value(1595880345, "0.0")
                                .value(1595880360, "0.0")
                                .value(1595880375, "0.0")      // interval
                                .value(1595880390, "0.0")
                                .value(1595880405, "0.0")
                                .value(1595880420, "0.0")
                                .value(1595880435, "0.0")      // interval
                                .value(1595880450, "0.0")
                                .value(1595880465, "0.0")
                                .value(1595880480, "0.0")
                                .value(1595880495, "0.0")      // interval
                                .value(1595880510, "5.0")
                                .value(1595880525, "5.0")
                                .value(1595880540, "5.0")
                                .value(1595880555, "5.0")      // interval
                                .value(1595880570, "0.0")
                                .value(1595880585, "0.0")
                                .value(1595880600, "0.0")
                                .value(1595880615, "0.0")      // interval
                                .value(1595880630, "0.0")
                                .value(1595880645, "0.0")
                                .value(1595880660, "0.0")
                                .value(1595880675, "0.0")      // interval
                                .build())
                        .build())
                .build());
    }

    @Test
    void shouldHandelCounterWithCalculationFluctuationDuringTimeShiftShifted2() {
        // Given
        // When
        PrometheusQueryRangeResponse response = restTemplate.getForObject(
                UriComponentsBuilder.fromHttpUrl("http://localhost:" + port + "/api/v1/query_range")
                        .queryParam("query", "lab_increase(request_duration_seconds_count[10 * 10s])")
                        .queryParam("start", "1596099190")
                        .queryParam("end", "1596100540")
                        .queryParam("step", "10")
                        .queryParam("_", "1592934520425")
                        .toUriString(),
                PrometheusQueryRangeResponse.class);

        // Then
        assertThat(response).isEqualTo(PrometheusQueryRangeResponse.builder()
                .status("success")
                .data(ResponseData.builder()
                        .resultType("matrix")
                        .result(ResponseResult.builder()
                                .metric("__name__", "request_duration_seconds_count")
                                .metric("instance", "some_instance")
                                .metric("label", "val1")
                                .metric("job", "some_job")
                                .value(1596099660, "3.0")
                                .value(1596099670, "3.0")
                                .value(1596099680, "3.0")
                                .value(1596099690, "3.0")
                                .value(1596099700, "12.0")
                                .value(1596099710, "12.0")
                                .value(1596099720, "12.0")
                                .value(1596099730, "12.0")
                                .value(1596099740, "12.0")
                                .value(1596099750, "12.0")
                                .value(1596099760, "12.0")
                                .value(1596099770, "12.0")
                                .value(1596099780, "12.0")
                                .value(1596099790, "12.0")
                                .value(1596099800, "7.0")
                                .value(1596099810, "7.0")
                                .value(1596099820, "7.0")
                                .value(1596099830, "7.0")
                                .value(1596099840, "7.0")
                                .value(1596099850, "7.0")
                                .value(1596099860, "7.0")
                                .value(1596099870, "7.0")
                                .value(1596099880, "7.0")
                                .value(1596099890, "7.0")
                                .value(1596099900, "0.0")
                                .value(1596099910, "0.0")
                                .value(1596099920, "0.0")
                                .value(1596099930, "0.0")
                                .value(1596099940, "0.0")
                                .value(1596099950, "0.0")
                                .value(1596099960, "0.0")
                                .value(1596099970, "0.0")
                                .value(1596099980, "0.0")
                                .value(1596099990, "0.0")
                                .value(1596100000, "0.0")
                                .value(1596100010, "0.0")
                                .value(1596100020, "0.0")
                                .value(1596100030, "0.0")
                                .value(1596100040, "0.0")
                                .value(1596100050, "0.0")
                                .value(1596100060, "0.0")
                                .value(1596100070, "0.0")
                                .value(1596100080, "0.0")
                                .value(1596100090, "0.0")
                                .value(1596100100, "0.0")
                                .value(1596100110, "0.0")
                                .value(1596100120, "0.0")
                                .value(1596100130, "0.0")
                                .value(1596100140, "0.0")
                                .value(1596100150, "0.0")
                                .value(1596100160, "0.0")
                                .value(1596100170, "0.0")
                                .value(1596100180, "0.0")
                                .value(1596100190, "0.0")
                                .value(1596100200, "0.0")
                                .value(1596100210, "0.0")
                                .value(1596100220, "0.0")
                                .value(1596100230, "0.0")
                                .value(1596100240, "0.0")
                                .value(1596100250, "0.0")
                                .value(1596100260, "0.0")
                                .value(1596100270, "0.0")
                                .value(1596100280, "0.0")
                                .value(1596100290, "0.0")
                                .value(1596100300, "0.0")
                                .value(1596100310, "0.0")
                                .value(1596100320, "0.0")
                                .value(1596100330, "0.0")
                                .value(1596100340, "0.0")
                                .value(1596100350, "0.0")
                                .value(1596100360, "0.0")
                                .value(1596100370, "0.0")
                                .value(1596100380, "0.0")
                                .value(1596100390, "0.0")
                                .value(1596100400, "0.0")
                                .value(1596100410, "0.0")
                                .value(1596100420, "0.0")
                                .value(1596100430, "0.0")
                                .value(1596100440, "0.0")
                                .value(1596100450, "0.0")
                                .value(1596100460, "0.0")
                                .value(1596100470, "0.0")
                                .value(1596100480, "0.0")
                                .value(1596100490, "0.0")
                                .value(1596100500, "0.0")
                                .value(1596100510, "0.0")
                                .value(1596100520, "0.0")
                                .value(1596100530, "0.0")
                                .value(1596100540, "0.0")
                                .build())
                        .build())
                .build());
    }

}
