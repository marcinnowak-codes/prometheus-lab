#!/bin/bash

val1=0
val1_increase=1
val1_interval=241
val2=1
val2_increase=2
val2_interval=373
val3=2
val3_increase=3
val3_interval=499
val4=5
val4_increase=5
val4_interval=641

iteration=0

for (( ; ; ))
do
    iteration=$((iteration + 1))
    echo -e "# TYPE some_counter_metric counter\nsome_counter_metric{device=\"car1\",country=\"Poland\",location=\"Warszawa\"} $val1\nsome_counter_metric{device=\"car1\",country=\"Poland\",location=\"Cracow\"} $val2\nsome_counter_metric{device=\"car1\",country=\"Germany\",location=\"Berlin\"} $val3\nsome_counter_metric{device=\"car1\",country=\"Germany\",location=\"Hamburg\"} $val4\n" \
      | curl --data-binary @- http://localhost:9091/metrics/job/some_job/instance/some_instance
    sleep 1
    if ((iteration % val1_interval == 0))
    then
      val1=$((val1 + val1_increase))
      echo "increase val1 to ${val1}"
    fi
    if ((iteration % val2_interval == 0))
    then
      val2=$((val2 + val2_increase))
      echo "increase val2 to ${val2}"
    fi
    if ((iteration % val3_interval == 0))
    then
      val3=$((val3 + val3_increase))
      echo "increase val3 to ${val3}"
    fi
    if ((iteration % val4_interval == 0))
    then
      val4=$((val4 + val4_increase))
      echo "increase val4 to ${val4}"
    fi
done
