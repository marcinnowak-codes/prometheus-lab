package codes.marcinnowak.prometheus.lab.webflux.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Validated
@Component
@ConfigurationProperties("prometheus-lab")
public class Properties {
    private PrometheusProperties prometheus;

    private SimulatorProperties simulator;

    @Data
    public static class SimulatorProperties{
        @NotNull
        private boolean enable;
    }

    @Data
    public static class PrometheusProperties{
        @NotEmpty
        private String baseUrl;
    }


}
