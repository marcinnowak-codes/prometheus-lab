package codes.marcinnowak.prometheus.lab.webflux.config;

import codes.marcinnowak.prometheus.lab.webflux.service.PrometheusService;
import codes.marcinnowak.prometheus.lab.webflux.simulator.PrometheusServiceSimulator;
import codes.marcinnowak.prometheus.lab.webflux.service.PrometheusServiceWebClientProxy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Component
public class BeanFactory {
    private final Properties properties;

    public BeanFactory(Properties properties) {
        this.properties = properties;
        log.info("Simulator is {}", properties.getSimulator().isEnable());
    }

    @Bean
    public PrometheusService createPrometheusService() {
        if (properties.getSimulator().isEnable())
            return new PrometheusServiceSimulator();

        WebClient client = WebClient.create(properties.getPrometheus().getBaseUrl());
        return new PrometheusServiceWebClientProxy(client, properties.getPrometheus().getBaseUrl());
    }
}
