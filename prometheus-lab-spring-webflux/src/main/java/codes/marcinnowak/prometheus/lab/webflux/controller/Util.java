package codes.marcinnowak.prometheus.lab.webflux.controller;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import org.springframework.web.util.UriComponentsBuilder;

public class Util {
    public static UriComponentsBuilder toQueryRangeUriBuilder(PrometheusQueryRequest queryRequest, String baseUrl) {
        return UriComponentsBuilder.fromHttpUrl(baseUrl + "/api/v1/query_range")
                .queryParam("query", queryRequest.getQuery())
                .queryParam("start", queryRequest.getStart())
                .queryParam("end", queryRequest.getEnd())
                .queryParam("step", queryRequest.getStep())
                .queryParam("_", 1592934520425L);
    }
}
