package codes.marcinnowak.prometheus.lab.webflux.service;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import reactor.core.publisher.Mono;

public interface PrometheusService {
    Mono<PrometheusQueryRangeResponse> queryRange(int step, PrometheusQueryRequest promQueryRange);
}
