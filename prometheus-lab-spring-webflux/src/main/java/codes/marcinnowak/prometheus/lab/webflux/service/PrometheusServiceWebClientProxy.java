package codes.marcinnowak.prometheus.lab.webflux.service;

import codes.marcinnowak.prometheus.lab.webflux.controller.Util;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotEmpty;
import java.net.URI;

public class PrometheusServiceWebClientProxy implements PrometheusService {
    private final WebClient client;
    private final String baseUrl;

    private static final String QUERY_RANGE = "queryRange";

    public PrometheusServiceWebClientProxy(WebClient client, @NotEmpty String baseUrl) {
        this.client = client;
        this.baseUrl = baseUrl;
    }

    @Override
    @Retry(name = QUERY_RANGE)
    public Mono<PrometheusQueryRangeResponse> queryRange(int step, PrometheusQueryRequest promQueryRange) {
        UriComponentsBuilder prometheusQueryRangeUri = Util.toQueryRangeUriBuilder(promQueryRange, baseUrl);

        final URI uri = prometheusQueryRangeUri
                .replaceQueryParam("step", step)
                .build()
                .encode()
                .toUri();

        return client.get()
                .uri(uri)
                .retrieve()
                .bodyToMono(PrometheusQueryRangeResponse.class);
    }
}
