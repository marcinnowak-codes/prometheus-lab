package codes.marcinnowak.prometheus.lab.webflux.simulator;

import codes.marcinnowak.prometheus.lab.simulator.PrometheusSimulator;
import codes.marcinnowak.prometheus.lab.simulator.PrometheusSimulatorRandom;
import codes.marcinnowak.prometheus.lab.webflux.service.PrometheusService;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import reactor.core.publisher.Mono;

public class PrometheusServiceSimulator implements PrometheusService {

//    private final PrometheusSimulator simulator = new PrometheusSimulatorStatic();
    private final PrometheusSimulator simulator = PrometheusSimulatorRandom.create(5);

    @Override
    public Mono<PrometheusQueryRangeResponse> queryRange(int step, PrometheusQueryRequest promQueryRange) {
        return Mono.just(simulator.queryRange(promQueryRange));
    }
}
