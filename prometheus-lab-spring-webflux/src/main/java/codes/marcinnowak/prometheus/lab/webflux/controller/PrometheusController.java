package codes.marcinnowak.prometheus.lab.webflux.controller;

import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRangeResponse;
import codes.marcinnowak.prometheus.lab.model.PrometheusQueryRequest;
import codes.marcinnowak.prometheus.lab.model.StatusResponse;
import codes.marcinnowak.prometheus.lab.promql.PQLParser;
import codes.marcinnowak.prometheus.lab.promql.PromDataRequest;
import codes.marcinnowak.prometheus.lab.promql.PromEvent;
import codes.marcinnowak.prometheus.lab.webflux.service.PrometheusService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;
import reactor.util.context.Context;

import java.math.BigDecimal;
import java.net.URLDecoder;

@RestController
public class PrometheusController {

    private final PrometheusService prometheusService;

    public PrometheusController(PrometheusService prometheusService) {
        this.prometheusService = prometheusService;
    }

    @RequestMapping("/api/v1/query")
    public StatusResponse queryRange(@RequestParam String query,
                                     @RequestParam BigDecimal time) {

        return StatusResponse.builder()
                .status("success")
                .data(StatusResponse.DataResponse.builder()
                        .resultType("scalar")
                        .result(time)
                        .result("2")
                        .build())
                .build();
    }

    @RequestMapping("/api/v1/query_range")
    public Mono<PrometheusQueryRangeResponse> queryRange(@RequestParam String query,
                                                         @RequestParam BigDecimal start,
                                                         @RequestParam BigDecimal end,
                                                         @RequestParam int step) {
        PrometheusQueryRequest queryRequest = PrometheusQueryRequest.builder()
                .query(URLDecoder.decode(query))
                .start(start)
                .end(end)
                .step(step)
                .build();

        if (!(query.contains("lab_increase") || (query.contains("lab_nincrease")))) {
            return prometheusService.queryRange(step, queryRequest);
        }

        PQLParser pqlParser = new PQLParser();

        pqlParser.handleIncreaseByVisitor(queryRequest);

        while (pqlParser.hasNext()) {
            PromEvent event = pqlParser.next();
            if (event instanceof PromDataRequest) {
                PromDataRequest promDataRequest = (PromDataRequest) event;
                return prometheusService.queryRange(promDataRequest.getStep(), PrometheusQueryRequest.builder()
                        .query(promDataRequest.getQuery())
                        .start(promDataRequest.getStart())
                        .end(promDataRequest.getEnd())
                        .step(promDataRequest.getStep())
                        .underline(promDataRequest.getUnderline())
                        .build())
                        .doOnEach(this::toMapper)
                        .subscriberContext(Context.of("parser", pqlParser));
            }
        }
        return Mono.empty();
    }

    private PrometheusQueryRangeResponse toMapper(Signal<PrometheusQueryRangeResponse> signal) {
        if (!signal.isOnNext())
            return null;

        PQLParser pqlParser = signal.getContext().get("parser");
        pqlParser.onDataRequestResponse(signal.get());
        while(pqlParser.hasNext()){
            pqlParser.next();
        }
        return pqlParser.getPrometheusQueryRangeResponse();
    }

}
